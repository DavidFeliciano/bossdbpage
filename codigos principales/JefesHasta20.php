<?php

$servername = "localhost";
$username = "root";
$password = "super3";

try {
    $conn = new PDO("mysql:host=$servername; dbname=thevideogamebossdatabase", $username, $password);
    $query = $conn->prepare("SELECT jefe.*, producto.ProductoNombre FROM jefe join producto on jefe.JuegoId = producto.id order by jefe.JefeId desc limit 20");
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($result);

} catch (PDOException $e) {
    echo json_encode("Connection failed: " . $e->getMessage());
}

?>